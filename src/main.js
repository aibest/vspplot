// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import http from './service/http'
import api from './service/api'
import './assets/reset.css'
import 'bootstrap'
import axios from 'axios'
import * as echarts from 'echarts'
// import qs from 'qs'
import Contextmenu from "vue-contextmenujs"
Vue.use(Contextmenu)
// Vue.prototype.$axios = axios
Vue.prototype.$http = http
Vue.prototype.$api = api
Vue.prototype.$echarts = echarts
// Vue.prototype.$qs=qs
import 'url-search-params-polyfill';
axios.defaults.baseURL = 'http://192.168.31.179:8080/ProductionSupport_war'
Vue.config.productionTip = false
Vue.use(ElementUI)
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>',
    axios
})