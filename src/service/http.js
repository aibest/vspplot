/**
 * axios 请求封装
 * 响应拦截 错误处理
 */
import axios from 'axios'
import Vue from 'vue'


let vm = new Vue()
axios.defaults.baseURL = '' //服务器地址
axios.defaults.withCredentials = true
axios.defaults.headers.post['Content-Type'] = 'multipart/form-data'
axios.defaults.timeout = 1500


export default {
    post(url, data) {
        return new Promise((resolve, reject) => {
            axios.post(url, data)
                .then(
                    res => resolve(res.data)
            )
                .catch(
                    err => reject(err)
            )
        })
    },
    get(url, params) {
        return new Promise((resolve, reject) => {
            axios.get(url, {
                params: params
            })
                .then(
                    res => resolve(res.data)
            )
                .catch(
                    err => reject(err)
            )
        })
    }
}