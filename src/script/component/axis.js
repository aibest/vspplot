/* ===================================================
 * 坐标基类
 * ===================================================*/

import util from '../util'

import axisBuilder from './axisBuilder'
util.inherits(Axis, Base);

var Axis = function(options, axisType) {
	this.axisType = axisType;
    this._axisList = [];

    
    this.refresh(options);
}
Axis.prototype = {
	constructor: Axis,
    
    // 更新视图
    refresh : function (newOption, newData) {

    },
    
    // 获取坐标
    getAxis : function (id) {
        return this._axisList[id];
    },
    // 清空_axisList[]
    clear : function () {

    },
    // 销毁
    dispose : function () {

    },
};