/* ===================================================
 * 组件基类
 * ===================================================*/
import util from '../util'

var Base = function (options) {
  this.options = options;
  this.shapeList = [];
  this.effectList = [];

}
Base.prototype = {
  constructor: Base,
  // 获取配置项属性
  _getAttr: function (attr) {

  },
  /**
   * 获取Canvas层级，zlevel 大的Canvas会放在zlevel小的Canvas的上面。
   */
  getZlevel: function () {
    return this._getAttr('zlevel');
  },
  /**
   * 获取图形层级，z值小的图形会被z值大的图形覆盖。z相比zlevel优先级更低，而且不会创建新的 Canvas。
   */
  getZ: function () {
    return this._getAttr('z');
  },
  // 配置项修改
  reformOption: function (opt) {
    
  },
  // 获取图元
  getShapeById: function (id) {

  },
  resize: function () {

  },
  // 清空shapeList[]
  clear: function () {

  },
  // 销毁
  dispose: function () {

  },
};
export default Base;