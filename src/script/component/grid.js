/* ===================================================
 * 网格基类
 * ===================================================*/
import * as zrender from "zrender";
import Base from './base.js';
import config from '../config'
config.grid = {
    zlevel: 0,                  
    z: 0,                       
    x: 80,
    y: 60,
    x2: 80,
    y2: 60,
    backgroundColor: 'rgba(0,0,0,0)',
    borderWidth: 1,
    borderColor: '#ccc'
};

zrender.util.inherits(Grid, Base);

var Grid = function(options) {
	this.refresh(options);
}
Grid.prototype = {
	constructor: Grid,
	
	getX : function () {
        return this._x;
    },

    getY : function () {
        return this._y;
    },

    getWidth : function () {
        return this._width;
    },

    getHeight : function () {
        return this._height;
    },

    getXend : function () {
        return this._x + this._width;
    },

    getYend : function () {
        return this._y + this._height;
    },

    getArea : function () {
        return {
            x : this._x,
            y : this._y,
            width : this._width,
            height : this._height
        };
    },
    refresh : function (newOption) {

    }
}