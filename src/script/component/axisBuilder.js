/* ===================================================
 * 坐标绘制器
 * ===================================================*/

import Base from './base.js';
import grid from './grid';
import config from '../config';
import * as zrender from "zrender";

zrender.util.inherits(AxisBuilder, Base);

config.axisBuider = {
  zlevel: 0,
  z: 0,
  show: true,
  position: 'left',      // 位置
  name: '',              // 坐标轴名字，默认为空
  // min: null,          // 最小值
  // max: null,          // 最大值
  // splitNumber: 5,     // 分割段数，默认为5
  axisLine: {            // 坐标轴线
    show: true,        // 默认显示，属性show控制显示与否
    onZero: true,
    lineStyle: {       // 属性lineStyle控制线条样式
      color: '#000',
      width: 2,
      type: 'solid'
    }
  },
  axisTick: {            // 坐标轴刻度
    show: false,       // 属性show控制显示与否，默认不显示
    length: 5,         // 属性length控制线长
    lineStyle: {       // 属性lineStyle控制线条样式
      color: '#000',
      width: 1
    }
  },
  axisLabel: {           // 坐标轴文本标签
    show: true,
    margin: 8,
    textStyle: {
      color: '#000'
    }
  },
  splitLine: {           // 分隔线
    show: true,        // 默认显示，属性show控制显示与否
    lineStyle: {       // 属性lineStyle控制线条样式
      color: ['#ccc'],
      width: 1,
      type: 'solid'
    }
  },
  splitArea: {           // 分隔区域
    show: false,       // 默认不显示，属性show控制显示与否
    areaStyle: {       // 属性areaStyle控制区域样式
      color: '#ccc'
    }
  }
};
var AxisBuilder = function (options, data) {
  if (!data || data.length === 0) {
    console.err('Invalid data!');
    return;
  }

  this.data = data;
  this.grid = grid;

  this.refresh(options, data);
}

AxisBuilder.prototype = {
  // 绘制
  _buildShape: function () {
    this._buildSplitArea();
    this._buildSplitLine();
    this._buildAxisLine();
    this._buildAxisTick();
    this._buildAxisLabel();
  },
  // 绘制坐标线
  _buildAxisLine: function () {

  },
  // 绘制坐标刻度
  _buildAxisTick: function () {
    if (this.isHorizontal()) {
      // 横向
    } else {
      // 纵向
    }
  },
  // 绘制坐标刻度文本
  _buildAxisLabel: function () {
    if (this.isHorizontal()) {
      // 横向
    } else {
      // 纵向
    }
  },
  // 绘制坐标刻度文本
  _buildSplitLine: function () {
    if (this.isHorizontal()) {
      // 横向
    } else {
      // 纵向
    }
  },
  _buildSplitArea: function () {
    if (this.isHorizontal()) {
      // 横向
    } else {
      // 纵向
    }
  },
  // 计算极值
  _calculateValue: function () {

  },

  // 获取极值
  getExtremum: function () {

  },

  // 根据值换算位置
  getCoord: function (value) {

  },
  // 根据位置换算值
  getValueFromCoord: function (coord) {

  },
  // 获取坐标轴定位
  getPosition: function () {

  },
  // 判断坐标轴是否是水平坐标轴
  isHorizontal: function () {

  },
  // 布局调整，纵向|横向
  refixAxisShape: function () {

  },
  // 更新视图
  refresh: function (newOption, newData) {

  },

}

export default AxisBuilder;