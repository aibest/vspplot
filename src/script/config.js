/* ===================================================
 * 配置项
 * ===================================================*/
var config = {
  // 图表类型
  CHART_TYPE_LINE: 'Log',
 

  // 组件类型
  COMPONENT_TYPE_AXIS: 'axis',
  COMPONENT_TYPE_GRID: 'grid',
  
  // 全图默认背景
  backgroundColor: 'rgba(0,0,0,0)',

  // 主题，主题
  textStyle: {
      decoration: 'none',
      fontFamily: 'Arial, Verdana, sans-serif',
      fontFamily2: '微软雅黑',    
      fontSize: 12,
      fontStyle: 'normal',
      fontWeight: 'normal'
  },

  EVENT: {
      REFRESH: 'refresh',
  },
 
};

export default config;