/* ===================================================
 * 剖面图模块
 * ===================================================*/
import axis from './axis';
import grid from './grid';
import config from '../config';
import Base from './base.js';
import * as zrender from "zrender";
zrender.util.inherits(Seismic, Base);
var Seismic = function(options) {
     this.refresh(options);

}

Seismic.prototype = {
	constructor: Seismic,

	/**
     * 初始化
     * @param  {} 
     * @return {}
     */
	init: function(dom, options) {

          // 画布初始化
          // add坐标轴
          // post获取图片
          // add图片
	},
	/**
     * 更新
     * @param  {} 
     * @return {}
     */
     refresh: function(data, index) {

	},

     
	/**
     * 平移
     * @param  {} 
     * @return {}
     */
	move: function() {

	},
	/**
     * 放大、缩小
     * @param  {} 
     * @return {}
     */
	scale: function(index) {

	}
	
};