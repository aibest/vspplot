/* ===================================================
 * 测井曲线模块
 * ===================================================*/

import axis from './axis';
import grid from './grid';
import config from '../config';
import Base from './base.js';

import * as zrender from "zrender";
zrender.util.inherits(Seismic, Base);

config.log = {
     zlevel: 0,                 
     z: 2,                      
     xAxisIndex: 0,
     yAxisIndex: 0,
     itemStyle: {
          normal: {
             // color: ,
             label: {
                 show: false
                 // position: 默认自适应，水平布局为'top'，垂直布局为'right'，可选为'left'|'right'|'top'|'bottom'
             },
             lineStyle: {
                 width: 2,
                 type: 'solid',
             }
         },
        
     },
 };
var Log = function(options) {
     this.refresh(options);
}

Log.prototype = {
	constructor: Log,

	/**
     * 初始化
     * @param  {} 
     * @return {}
     */
	init: function(dom, options) {

	},
	/**
     * 更新
     * @param  {} 
     * @return {}
     */
    refresh: function(options) {

	},
	/**
     * 平移
     * @param  {} 
     * @return {}
     */
	move: function() {

	},
	/**
     * 放大、缩小
     * @param  {} 
     * @return {}
     */
	scale: function(index) {

	}
	
};