/* eslint-disable no-new */

import Vue from 'vue'
import Router from 'vue-router'
// import Calibration from '@/views/Calibration/Index'
// import Profile from '@/views/Profile/Index'
// import Test from '@/components/Test'
import Index from '@/views/Index'
import Contain from '@/views/Contain'
Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Index
        },
        {
            path: '/contain',
            name: 'Contain',
            component: Contain
        },
    ]
})